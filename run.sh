#!/bin/bash
cargo b --release
sudo setcap cap_net_admin=eip $CARGO_TARGET_DIR/release/tcp-rs
$CARGO_TARGET_DIR/release/tcp-rs &
pid=$!
sudo ip addr add 192.168.32.1/24 dev tun0
sudo ip link set up tun0
trap "kill $pid" INT TERM
wait $pid
