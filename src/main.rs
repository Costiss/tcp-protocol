use std::collections::HashMap;
use std::io;
use std::net::Ipv4Addr;

extern crate tun_tap;

#[derive(Clone, Debug, Hash, PartialEq, Eq)]

struct TcpState {}

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
struct Conn {
    source: (Ipv4Addr, u16),
    destination: (Ipv4Addr, u16),
}

#[allow(unreachable_code)]
fn main() -> io::Result<()> {
    let connections: HashMap<Conn, TcpState> = HashMap::new();

    let nic = tun_tap::Iface::new("tun0", tun_tap::Mode::Tun)?; //generates tun conn
    let mut buf = [0u8; 1504]; //

    loop {
        let nbytes = nic.recv(&mut buf[..])?; //read from tun conn

        // Data Link layer parsing
        let _eth_flags = u16::from_be_bytes([buf[0], buf[1]]);
        let eth_proto = u16::from_be_bytes([buf[2], buf[3]]);

        if eth_proto != 0x0800 {
            continue; //checks for IPV4 Only
        }

        match etherparse::Ipv4HeaderSlice::from_slice(&buf[4..nbytes]) {
            Ok(ip_packet) => {
                let src = ip_packet.source_addr();
                let dest = ip_packet.destination_addr();
                let protocol = ip_packet.protocol();
                if protocol == 0x01 {
                    println!("Received a ping from: {}", src)
                }
                if protocol != 0x06 {
                    continue; //Not TCP
                }

                match etherparse::TcpHeaderSlice::from_slice(&buf[4 + ip_packet.slice().len()..]) {
                    Ok(tcp) => {
                        println!(
                            "{} - {}: {}b of TCP to port {}",
                            src,
                            dest,
                            tcp.slice().len(),
                            tcp.destination_port(),
                        );
                    }
                    Err(e) => println!("Not an TCP packet: {:?}", e),
                }
            }
            Err(e) => println!("Not an IP packet: {:?}", e),
        };
    }
    Ok(())
}
